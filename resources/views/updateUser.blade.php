@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Todo List</div>

                <div class="panel-body">
                    <table class="table">
                        <th>id</th>
                        <th>Name</th>
                        <th>Action</th>
                        @foreach($users as $uesr)
                        <tr>
                            <td>{{$uesr->id}}</td>
                            <td>{{$uesr->name}}</td>
                            <td>
                                <select name="type">
                                    @foreach($user_types as $user_type)
                                    <option value="{{$uesr->user_type}}> {{$uesr->user_type}}</option>
                                    @endforeach
                                </select>
                                
                            </td>
                            <td><a href="#">Update</a></td>
                            
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
