@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Todo</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('storeEditTodo') }}">
                        {{ csrf_field() }}
                        <input type="text" name="id" hidden="" value="{{$id}}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="title" value="{{ $info->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('create_by') ? ' has-error' : '' }}">
                            <label for="create_by" class="col-md-4 control-label">Create By</label>

                            <div class="col-md-6">
                                <input id="create_by" type="text" class="form-control" name="create_by" value="{{ $info->created_by }}"  required autofocus>

                                @if ($errors->has('create_by'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('create_by') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                     

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Edit Todo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
