<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/userRegistration', 'RegistrationController@registration')->name('registration');
Route::post('/storeRegistration', 'RegistrationController@storeRegistration')->name('storeRegistration');
Route::post('/login', 'RegistrationController@login')->name('login');

Route::get('/welcomeView', 'RegistrationController@welcomeView')->name('welcomeView');
Route::get('/createTodo', 'RegistrationController@createTodo')->name('createTodo');
Route::get('/allTodo', 'RegistrationController@allTodo')->name('allTodo');
Route::get('/deletedTodo', 'RegistrationController@deletedTodo')->name('deletedTodo');
Route::post('/storeTodo', 'RegistrationController@storeTodo')->name('storeTodo');

Route::get('/editTodo', 'RegistrationController@editTodo')->name('editTodo');

Route::get('/deleteTodo', 'RegistrationController@deleteTodo')->name('deleteTodo');
Route::post('/storeEditTodo', 'RegistrationController@storeEditTodo')->name('storeEditTodo');

Route::get('/updateUser', 'RegistrationController@updateUser')->name('updateUser');

Route::get('/editUser', 'RegistrationController@editUser')->name('editUser');