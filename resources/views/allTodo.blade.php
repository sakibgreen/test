@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Todo List</div>

                <div class="panel-body">
                    <table class="table">
                        <th>id</th>
                        <th>Title</th>
                        <th>Create By</th>
                        <th>Action</th>
                        @foreach($allTodos as $todo)
                        <tr>
                            <td>{{$todo->id}}</td>
                            <td>{{$todo->title}}</td>
                            <td>{{$todo->created_by}}</td>
                            <td><a href="{{route('editTodo',['id'=>$todo->id])}}">Edit</a></td>
                            @if($todo->user_type=='Regular_User' || $todo->user_type=='Admin')
                            <td><a href="{{route('deleteTodo',['id'=>$todo->id])}}">Delete</a></td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
