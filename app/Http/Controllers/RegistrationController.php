<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Todo;
use Session;

class RegistrationController extends Controller
{
    public function registration(){
    	if(Session::has('email')){
    		return redirect()->route('welcomeView');
    	}
    	// dd('test');
    	return view('registration');
    }

    public function storeRegistration(Request $request){
    	$registration = new registration;
    	$registration->name = $request->get('name');
    	$registration->email = $request->get('email');
    	$registration->password = $request->get('password');
    	$registration->save();
    	return redirect()->route('login');
    }



	public function login(Request $request)
	{
		$email = $request->get('email');
		$password = $request->get('password');
		$checkuser = Registration::where(['email'=>$email, 'password'=>$password])->first();
		//dd($checkuser->name);
		Session::put('email',$email);
		Session::put('name',$checkuser->name);
		return redirect()->route('welcomeView');

	}

	public function welcomeView(Request $request)
	{
		if(Session::has('email')){
			$email = Session::get('email');
			$registeredUser= Registration::where('email', $email)->first();
			$user_type = $registeredUser->user_type;
			if($user_type=='Regular_User'){
				Session::put('type',$user_type);
				return view('welcomeView');
			}
			else{
				return view('welcomeView');
			}
			
		}
		
	}

	public function createTodo(Request $request)
	{
		if(Session::has('email')){

			return view('createTodo');
		}
		
	}

	public function allTodo(Request $request)
	{
		if(Session::has('email')){
			$email = Session::get('email');
			$registeredUser= Registration::where('email', $email)->first();
			$user_type = $registeredUser->user_type;
			if($user_type=='Admin'){
				$allTodos= Todo::all();
			}
			elseif ($user_type=='Regular_User') {
				$allTodos = Todo::where('user_type',$user_type)->get();
				
			}

			
			// dd($allTodos);
			return view('allTodo',['allTodos'=>$allTodos]);
		}
		
	}

	public function storeTodo(Request $request){
		$email = Session::get('email');
		$registeredUser= Registration::where('email', $email)->first();
		$user_type = $registeredUser->user_type;
		// dd($user_type);
		$name= Session::get('name');
    	$todo = new Todo;
    	$todo->title = $request->get('title');
    	$todo->created_by = $name;
    	$todo->email = $email;
    	$todo->user_type = $user_type;
    	$todo->save();
    	return redirect()->route('welcomeView');
    }

    public function storeEditTodo(Request $request){
    	$id= $request->id;
		$email = Session::get('email');
		$registeredUser= Registration::where('email', $email)->first();
		$user_type = $registeredUser->user_type;
		// dd($user_type);
		$name= Session::get('name');

		$editedTodo = Todo::where('id',$id)->first();

    	$editedTodo->title = $request->get('title');
    	$editedTodo->created_by = $name;
    	$editedTodo->email = $email;
    	$editedTodo->user_type = $user_type;
    	$editedTodo->save();
    	return redirect()->route('welcomeView');
    }
    
    public function editTodo(Request $request){
    	$id =$request->get('id');
    	$info = Todo::where('id', $id)->first();
    	return view('editTodo', ['id'=>$id,'info'=>$info ]);
    }

    public function deleteTodo(Request $request){
    	$id =$request->get('id');
    	$info = Todo::where('id', $id)->first();
    	$info->delete($info->id);
		return redirect()->route('allTodo');
    }

 public function updateUser(Request $request){
 		$user_types = Registration::all();
 		//dd($user_types);
    	$users = Registration::where('user_type','Regular_User')->get();
    	return view('updateUser', ['users'=>$users,'user_types'=>$user_types]);
    }

    public function editUser(Request $request){
    	
    }


}
